### Shell history

HISTFILE=~/.histfile
HISTSIZE=10000
SAVEHIST=10000

### Other zsh options

setopt AUTO_CD

### Key bindings

bindkey -v
bindkey '^A' vi-beginning-of-line
bindkey '^E' vi-end-of-line
bindkey '^P' vi-up-line-or-history
bindkey '^N' vi-down-line-or-history
bindkey '^R' history-incremental-search-backward

### Completion

autoload -Uz compinit
compinit

if [[ -f /usr/share/zsh/plugins/zsh-syntax-highlighting/zsh-syntax-highlighting.zsh ]]; then
    # Arch
    source /usr/share/zsh/plugins/zsh-syntax-highlighting/zsh-syntax-highlighting.zsh
elif [[ -f /usr/share/zsh-syntax-highlighting/zsh-syntax-highlighting.zsh ]]; then
    # Debian
    source /usr/share/zsh-syntax-highlighting/zsh-syntax-highlighting.zsh
fi

# Enable 'v' to edit command line
autoload -U edit-command-line
zle -N edit-command-line
bindkey -M vicmd v edit-command-line

# Activate Starship prompt
eval "$(starship init zsh)"

### Functions

# Give man some color
function man {
    env LESS_TERMCAP_mb=$'\E[01;31m' \
    LESS_TERMCAP_md=$'\E[01;38;5;74m' \
    LESS_TERMCAP_me=$'\E[0m' \
    LESS_TERMCAP_se=$'\E[0m' \
    LESS_TERMCAP_so=$'\E[38;5;246m' \
    LESS_TERMCAP_ue=$'\E[0m' \
    LESS_TERMCAP_us=$'\E[04;38;5;146m' \
    man "$@"
}

# Replacement for pushd
function push {
    pushd "${@}" >/dev/null;
    dirs -v;
}

# Replacement for popd
function pop {
    popd "${@}" >/dev/null;
    dirs -v;
}

function bm {
    if [[ -z "$1" ]]; then
        if [[ `whence exa` ]]; then
            exa -1 "$HOME/.bookmarks"
        else
            ls -o --color=always "$HOME/.bookmarks"
        fi
    else
        local d=`readlink -n $HOME/.bookmarks/${1}`
        echo "$d"
        cd "$d"
    fi
}

function up {
    local limit="$1"
    if [ -z "$limit" ] || [ "$limit" -le 0 ]; then
        limit=1
    fi
    local d=""
    for ((i=1; i <= limit; i++)); do
        d="../$d"
    done
    cd "$d"
}

function dtime {
    local tstamp=`date '+<t:%s:F>' -d $1`
    echo -n $tstamp | xclip -selection clipboard
    echo $tstamp
}

function mkcd {
    mkdir "$1"
    cd "$1"
}

function n {
    # Block nesting of nnn in subshells
    [ "${NNNLVL:-0}" -eq 0 ] || {
        echo "nnn is already running"
        return
    }

    # The behaviour is set to cd on quit (nnn checks if NNN_TMPFILE is set)
    # If NNN_TMPFILE is set to a custom path, it must be exported for nnn to
    # see. To cd on quit only on ^G, remove the "export" and make sure not to
    # use a custom path, i.e. set NNN_TMPFILE *exactly* as follows:
    NNN_TMPFILE="${XDG_CONFIG_HOME:-$HOME/.config}/nnn/.lastd"
    # export NNN_TMPFILE="${XDG_CONFIG_HOME:-$HOME/.config}/nnn/.lastd"

    # Unmask ^Q (, ^V etc.) (if required, see `stty -a`) to Quit nnn
    # stty start undef
    # stty stop undef
    # stty lwrap undef
    # stty lnext undef

    # The command builtin allows one to alias nnn to n, if desired, without
    # making an infinitely recursive alias
    command nnn -dRu "$@"

    [ ! -f "$NNN_TMPFILE" ] || {
        . "$NNN_TMPFILE"
        rm -f -- "$NNN_TMPFILE" > /dev/null
    }
}

### Aliases

# exa/ls
if [[ `whence eza` ]]
then
    alias eza='eza --binary --color=always'
    alias l='eza --long --group-directories-first --time-style=long-iso'
    alias la='l --all'
    alias lt='l --sort=modified'
    alias lta='lt --all'
    alias lg='l --git'
    alias lga='lg --all'
else
    alias l='ls -l --human-readable --group-directories-first --time-style=long-iso --color=always'
    alias la='l --almost-all'
    alias lt='l --sort=time --reverse'
    alias lta='lt --almost-all'
fi

# Change defaults
alias mv='mv -i'
alias cp='cp -i'
alias ln='ln -i'
alias rm='rm -I'
alias mkdir='mkdir -pv'
alias dirs='dirs -v'
alias sed='sed --follow-symlinks'
alias ranger='ranger --choosedir=$HOME/.rangerdir; cd `cat $HOME/.rangerdir`'
alias df='df -h'
alias du='du -sh'
alias dart='dart --disable-analytics'
alias bc='bc -l'

if [[ -f /usr/bin/batcat ]]; then
    # Debian
    alias bat=batcat
fi

# For git command aliases see ~/.gitconfig
alias g='git s'
alias gl='git l'
alias glt='git lt'
alias glf='git lf'
alias gd='git d'
alias gdc='git dc'
alias gdt='git dt'
alias gdtc='git dtc'
alias gco='git co'
alias gw='git w'
alias ga='git a'
alias gu='git u'
alias gap='git ap'
alias gss='git ss'
alias gsl='git sl'
alias gsa='git sa'
alias gsd='git sd'
alias gsp='git sp'

# New commands
alias c='cd ~; clear'
alias e=nvim
alias ipy='ipython'
alias za='zathura'
alias qmvn='mvn -Dmaven.javadoc.skip=true -Dmaven.test.skip=true'
alias vt='kitty --detach --single-instance'
alias sxiv=nsxiv
alias iv=nsxiv
alias dockerps='docker ps --format "table {{.ID}}\t{{.Names}}\t{{.Image}}\t{{.Status}}"'
alias clear_mx822="http GET 'http://lex-mx822/esf/prtappse/semenu?page=clearlog&clearlogconfirmation=Yes'"
alias clear_cx921="http GET 'http://lex-cx921/esf/prtappse/semenu?page=clearlog&clearlogconfirmation=Yes'"
alias swipe_cx921="http POST 'http://lex-cx921/esf/prtapp/apps/fakeiddevice?action=cardswipe&cardId=041C127AC36980'"
alias devservice="ssh -t gbd-lv-devservice.geniusbytes.com tmux new-session -A -s main"
alias devservice2="ssh -t gbd-lv-devservice2.geniusbytes.com tmux new-session -A -s main"
alias devservice3="ssh -t gbd-lv-devservice3.geniusbytes.com tmux new-session -A -s main"
alias nexus3="ssh -t nexus3.geniusbytes.com tmux new-session -A -s main"
alias dotfiles='/usr/bin/git --git-dir=$HOME/.dotfiles.git/ --work-tree=$HOME'

### Suffix aliases

alias -s gif=nsxiv
alias -s htm=firefox
alias -s html=firefox
alias -s java=$EDITOR
alias -s jpeg=nsxiv
alias -s jpg=nsxiv
alias -s json=$EDITOR
alias -s jsonl=$EDITOR
alias -s log=lnav
alias -s mkv=mpv
alias -s mp4=mpv
alias -s pdf=zathura
alias -s png=nsxiv
alias -s properties=$EDITOR
alias -s svg=inkview
alias -s txt=$EDITOR
alias -s xml=$EDITOR
alias -s yaml=$EDITOR
alias -s yml=$EDITOR

### Pyenv initialization
export PYENV_ROOT="$HOME/.pyenv"
command -v pyenv >/dev/null || export PATH="$PYENV_ROOT/bin:$PATH"
eval "$(pyenv init -)"
eval "$(pyenv virtualenv-init -)"

### fzf initialization and config
source <(fzf --zsh)
export FZF_DEFAULT_COMMAND="fd --hidden --strip-cwd-prefix --exclude .git"
export FZF_CTRL_T_COMMAND="$FZF_DEFAULT_COMMAND"
export FZF_ALT_C_COMMAND="fd --type=d --hidden --strip-cwd-prefix --exclude .git"
function _fzf_compgen_path {
    fd --hidden --exclude .git . "$1"
}
function _fzf_compgen_dir {
    fd --type=d --hidden --exclude .git . "$1"
}

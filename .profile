prepend_path () {
    case ":$PATH:" in
        *:"$1":*)
            ;;
        *)
            PATH="$1${PATH:+:$PATH}"
    esac
}
prepend_path "$HOME/.local/bin"
prepend_path "$HOME/bin"
unset -f prepend_path
export PATH

export LANG="en_US.UTF-8"
export LC_MEASUREMENT="de_DE.UTF-8"
export LC_MESSAGES="C"
export LC_PAPER="de_DE.UTF-8"
export LC_TIME="en_DK.UTF-8"
export EDITOR="nvim"
export PAGER="less"
export LESS="-Ri"
export XDG_CONFIG_HOME="$HOME/.config"
export M2_REPO="~/.m2/repository"
export CUPS_USER=bernhard
export PIPENV_VENV_IN_PROJECT=1
export TERMCMD=kitty
export NNN_BMS="p:$HOME/documents/genius_bytes/dev/projects;v:$HOME/downloads/Videos"
export ANDROID_HOME="$HOME/Android/Sdk"
# export ANDROID_SDK_ROOT="$ANDROID_HOME"
export ANDROID_BTOOLS_VERSION="$ANDROID_HOME/build-tools/35.0.0"
export ANT_HOME="/usr/share/ant"
export JAVA_HOME="/usr/lib/jvm/default-runtime"

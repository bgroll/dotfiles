-- Gruvbox (colorscheme)
return {
    'ellisonleao/gruvbox.nvim',
    lazy = true,
    priority = 1000,
    opts = {},
}

-- Lualine (status line)
return {
    'nvim-lualine/lualine.nvim',
    dependencies = {
        'nvim-tree/nvim-web-devicons',
    },
    opts = {
        options = {
            theme = 'gruvbox',
            path = 4, -- Filename and parent dir, with tilde as the home directory
        }
    },
}

-- Gitsigns (git decorations)
return {
    'lewis6991/gitsigns.nvim',
    -- opts = {},
    config = function()
        require('gitsigns').setup()
        vim.keymap.set('n', '<leader>gb', ':Gitsigns toggle_current_line_blame<CR>')
        vim.keymap.set('n', '<leader>gd', ':Gitsigns preview_hunk<CR>')
    end,
}

-- NVim-Nu (support for the nushell language)
return {
    'LhKipp/nvim-nu',
    dependencies = {
        'nvim-treesitter/nvim-treesitter',
    },
    build = ':TSInstall nu',
    ft = 'nu',
}

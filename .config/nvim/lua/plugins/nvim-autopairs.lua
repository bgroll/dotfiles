-- NVim-Autopairs (auto-complete brackets, quotes)
return {
    'windwp/nvim-autopairs',
    event = 'InsertEnter',
    opts = {},
}

-- Telescope (fuzzy finder)
return {
    'nvim-telescope/telescope.nvim',
    dependencies = {
        'nvim-lua/plenary.nvim',
    },
    keys = {
        '<leader>b',
        '<leader>c',
        '<leader>f',
        '<leader>h',
        '<leader>m',
        '<leader>r',
    },
    config = function()
        local builtin = require('telescope.builtin')
        vim.keymap.set('n', '<leader>b', builtin.buffers)
        vim.keymap.set('n', '<leader>c', function()
            builtin.colorscheme({enable_preview = true})
        end)
        vim.keymap.set('n', '<leader>f', function()
            builtin.find_files({hidden = true})
        end)
        vim.keymap.set('n', '<leader>h', builtin.help_tags)
        vim.keymap.set('n', '<leader>m', builtin.marks)
        vim.keymap.set('n', '<leader>r', builtin.oldfiles)
    end,
}

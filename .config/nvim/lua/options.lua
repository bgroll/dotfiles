local opt = vim.opt

-- Buffer handling

opt.confirm = true -- ask before losing unsaved changes
opt.undofile = true -- allow undo from before a buffer was loaded

-- Tab handling and indentation

opt.tabstop = 4 -- tab characters count as 4 spaces
opt.shiftwidth = 4 -- indent 4 spaces per level
opt.shiftround = true -- round indents to multipes of shiftwidth
opt.expandtab = true -- hitting the tab key inputs spaces

-- Command-line completion

opt.wildmode = 'longest:full' -- complete as much as possible, then open wildmenu
opt.suffixes:append({'.class', '.pyc'}) -- lower match priority for class files, pyc files

-- Search

opt.ignorecase = true -- ignore case for search
opt.smartcase = true -- override ignorecase if pattern contains upper-case chars
opt.grepprg = 'rg --vimgrep --no-heading' -- use `rg` for :grep

-- Display options

opt.number = true -- show line numbers
opt.linebreak = true -- soft wrap only at whitespace or suitable character
opt.list = true -- show some whitespace characters
opt.listchars = { -- whitespace display characters
    tab = '⇥ ',
    nbsp = '·',
    trail = '␣',
    extends = '▸',
    precedes = '◂',
}
opt.display:append('uhex') -- show unprintable characters as hex values
opt.foldmethod = 'syntax' -- use buffer syntax to determine fold

-- Color scheme
vim.cmd('colorscheme gruvbox')

-- Fonts
vim.o.guifont = "Source Code Pro:h14"
vim.g.neovide_scale_factor = 0.5

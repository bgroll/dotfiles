local opts = {silent = true}

-- Leader key combos for frequent buffer and window commands
vim.keymap.set('n', '<leader>o', '<cmd>buffer #<cr>', opts)
vim.keymap.set('n', '<leader>d', '<cmd>bdelete<cr>', opts)
vim.keymap.set('n', '<leader>w', '<cmd>update<cr>', opts)
vim.keymap.set('n', '<leader>q', '<cmd>quit<cr>', opts)

-- Leader key combos for system clipboard operations
vim.keymap.set('v', '<leader>y', '"+y', opts)
vim.keymap.set('v', '<leader>d', '"+d', opts)
vim.keymap.set({'n', 'v'}, '<leader>p', '"+p', opts)
vim.keymap.set({'n', 'v'}, '<leader>P', '"+P', opts)

-- Remove search highlighting
vim.keymap.set('n', '<C-L>', '<cmd>nohlsearch<cr>', opts)

-- Set up the leader key before loading any plugins
vim.g.mapleader = ' '

require 'lazy-config'
require 'keymaps'
require 'options'
